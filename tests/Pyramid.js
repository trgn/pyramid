define(['pyr/Pyramid'], function (Pyramid) {

  module('Pyramid');

  test('t', function () {
    ok(true);
  });

  test('put (level)', function () {

    pyr = new Pyramid();

    a = {};
    pyr.put(0, 0, 0, a);

    g = pyr.get(0, 0, 0);

    equal(g, a);

  });

  test('put (1) ', function () {

    pyr = new Pyramid();

    a = {};
    pyr.put(1, 0, 0, a);
    g = pyr.get(1, 0, 0);
    equal(g, a);

    a = {};
    pyr.put(1, 1, 0, a);
    g = pyr.get(1, 1, 0);
    equal(g, a);

    a = {};
    pyr.put(1, 0, 1, a);
    g = pyr.get(1, 0, 1);
    equal(g, a);

    a = {};
    pyr.put(1, 1, 1, a);
    g = pyr.get(1, 1, 1);
    equal(g, a);

    a = {};
    pyr.put(0, 0, 0, a);
    g = pyr.get(0, 0, 0);
    equal(g, a);
  });

  test('put (2)', function () {

    pyr = new Pyramid();

    console.log('--------------------');
    a = {l: 2, x: 2, y: 0};
    pyr.put(2, 2, 0, a);
    g = pyr.get(2, 2, 0);
    equal(g, a);

    console.log('--------------------');
    a = {l: 1, x: 0, y: 0};
    pyr.put(1, 0, 0, a);
    g = pyr.get(1, 0, 0);
    equal(g, a);

    console.log('--------------------');
    a = {l: 2, x: 2, y: 3};
    pyr.put(2, 2, 3, a);
    g = pyr.get(2, 2, 3);
    equal(g, a);

    console.log('--------------------');
    a = {l: 1, x: 1, y: 0};
    pyr.put(1, 1, 0, a);
    g = pyr.get(1, 1, 0);
    equal(g, a);

    console.log('--------------------');
    a = {l: 1, x: 0, y: 1};
    pyr.put(1, 0, 1, a);
    g = pyr.get(1, 0, 1);
    equal(g, a);

    console.log('--------------------');
    var a111 = {l: 1, x: 1, y: 1};
    pyr.put(1, 1, 1, a111);
    g = pyr.get(1, 1, 1);
    equal(g, a111);

    console.log('--------------------');
    a = {l: 0, x: 0, y: 0};
    pyr.put(0, 0, 0, a);
    g = pyr.get(0, 0, 0);
    equal(g, a);

    console.log('--------------------');
    a213 = {l: 2, x: 1, y: 3};
    pyr.put(2, 1, 3, a213);
    g = pyr.get(2, 1, 3);
    equal(g, a213);

    console.log('--------------------');
    g = pyr.get(1, 1, 1);
    equal(g, a111);

    console.log('--------------------');
    pyr.remove(1, 1, 1);

    console.log('--------------------');
    g = pyr.get(1, 1, 1);
    equal(g, undefined);

    console.log('--------------------');
    pyr.remove(0, 0, 0);

    console.log('--------------------');
    g = pyr.get(0, 0, 0);
    equal(g, undefined);

    console.log('--------------------');
    g = pyr.get(2, 1, 3);
    console.log(g);
    console.log(pyr);
    equal(g, a213);

  });

  test('put (3)', function () {

    pyr = new Pyramid();

    console.log('--------------------');
    a220 = {l: 2, x: 2, y: 0};
    a100 = {l: 1, x: 0, y: 0};
    a223 = {l: 2, x: 2, y: 3};
    a110 = {l: 1, x: 1, y: 0};
    a101 = {l: 1, x: 0, y: 1};
    a111 = {l: 1, x: 1, y: 1};
    a000 = {l: 0, x: 0, y: 0};
    a213 = {l: 2, x: 1, y: 3};
    a203 = {l: 2, x: 0, y: 3};

    pyr.put(2, 1, 3, a213);
    pyr.put(0,0,0,a000);
    pyr.put(1,1,1,a111);

    ok(true);

  });


  function average(acc, value) {
    "use strict";
    var n = acc.weight + 1;
    acc.average = ((n - 1) * acc.average) / n + (value / n);
    acc.weight = n;
    return acc;
  }

  [3, 3, 3].reduce(average, {average: 0, weight: 0});


});