({
  baseUrl: './src',
  dir: './release/amd',
  modules: [
    {
      name: 'URLImagePyramid'
    }
  ],
  paths: {
    chopchop: "../lib/chopchop",
    jquery: 'bower_components/jquery/dist/jquery',
    Evented: 'bower_components/evented/Evented',
    RenderLoop: 'bower_components/renderloop/RenderLoop'
			
  },
  optimize: 'none',
  removeCombined: true,
  skipModuleInsertion: true
})
