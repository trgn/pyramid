define([
    './Pool'
], function (Pool) {

    "use strict";

    function sizeAtLevel(level, base) {
        return Math.pow(base, level);
    }

    function parentIndex(index, base) {
        return Math.floor(index / base);
    }

    function rangeAtDeeperLevel(higherLevel, index, base, deeperLevel, out_) {
        var range = Math.pow(base, deeperLevel - higherLevel);
        var from = index * range;
        out_.from = from;
        out_.to = from + range;
    }

    function coordinateInChild(base, l, c) {
        return  c % Math.pow(base, l - 1);
    }

    function nextSearchIndex(base, leveldiff, v) {
        return Math.floor(v / Math.pow(base, leveldiff - 1));
    }

    function keepBestEntry(bestentry, node) {
        return node._entry || bestentry;
    }

    function Pyramid(cols, rows) {

        this._xDiv = cols || 2;//nr of child tiles horizontally
        this._yDiv = rows || 2;//nr of child tiles vertically

        this._children = new Array(this._xDiv * this._yDiv);
        this._entry = undefined;

        this._parent = null;
        this._childCount = 0;

        this._nextInPool = null;
        this._pyramidPool = new Pool();

    }

    Pyramid.prototype = {

        constructor: Pyramid,

        _toChildIndex: function (x, y) {
            return (this._xDiv * x) + y;
        },

        _pruneBranch: function () {

            var parentNode = this._parent;
            var child = this;
            var ci;
            while (parentNode) {

                if (child._childCount > 0 || child._entry !== undefined) {
                    //child is not empty, so cannot prune branch.
                    return;
                }

                ci = parentNode._children.indexOf(child);
                parentNode._children[ci] = undefined;
                parentNode._childCount -= 1;

                child._parent = null;
                child._pyramidPool = null;
                parentNode._pyramidPool.putBack(child);

                //setup frame iteration
                child = parentNode;
                parentNode = parentNode._parent;

            }

        },

        reduceAllInRange: function (level, fromCol, toCol, fromRow, toRow, fold, accum) {

        },

        reducePath: function (l, x, y, fold, accum) {

            var node = this;
            var ind;
            while (true) { //two break conditions in the body.

                accum = fold(accum, node._entry);

                if (l === 0) {//(1) reached the desired level.
                    break;
                }

                //setup frame child
                ind = node._toChildIndex(
                    nextSearchIndex(this._xDiv, l, x),
                    nextSearchIndex(this._yDiv, l, y));
                node = node._children[ind];

                if (!node) {//(2) no more child nodes to explore.
                    break;
                }

                //setup frame coordinate
                x = coordinateInChild(this._xDiv, l, x);
                y = coordinateInChild(this._yDiv, l, y);
                l -= 1;

            }

            return accum;

        },

        search: function (l, x, y) {
            return this.reducePath(l, x, y, keepBestEntry, undefined);
        },

        remove: function (l, x, y) {

            var node = this;
            var sx, sy, ind;
            while (true) { //two return statements in the body.

                if (l === 0) {//(1) reached the tile to remove.
                    node._entry = undefined;
                    node._pruneBranch();
                    return;
                }

                //setup frame child
                sx = nextSearchIndex(node._xDiv, l, x);
                sy = nextSearchIndex(node._yDiv, l, y);
                ind = node._toChildIndex(sx, sy);
                node = node._children[ind];

                if (!node) {//(2) no more child nodes to explore. won't be able to find the tile.
                    return;
                }

                //setup frame coordinate
                x = coordinateInChild(this._xDiv, l, x);
                y = coordinateInChild(this._yDiv, l, y);
                l -= 1;

            }
        },

        get: function (l, x, y) {

            var node = this;
            var sx, sy, ind;
            while (true) { //two return statements in the body.

                if (l === 0) {//(1) reached the desired level.
                    return node._entry;
                }

                //setup frame child
                sx = nextSearchIndex(node._xDiv, l, x);
                sy = nextSearchIndex(node._yDiv, l, y);
                ind = node._toChildIndex(sx, sy);
                node = node._children[ind];

                if (!node) {//(2) no more child nodes to explore. won't be able to find the tile.
                    return;
                }

                //setup frame coordinate
                x = coordinateInChild(this._xDiv, l, x);
                y = coordinateInChild(this._yDiv, l, y);
                l -= 1;

            }

        },

        put: function (l, x, y, entry) {

            var node = this;
            var sx, sy, ind, child;
            while (true) { //single return statements in the body.

                if (l === 0) {//(1) reached the desired tile.
                    node._entry = entry;
                    return;
                }

                //do we have a child?
                sx = nextSearchIndex(node._xDiv, l, x);
                sy = nextSearchIndex(node._yDiv, l, y);
                ind = node._toChildIndex(sx, sy);
                child = node._children[ind];

                if (!child) {//(2) must created child so we can continue path.
                    child = node._spawnChild();
                    node._children[ind] = child;
                    node._childCount += 1;
                }

                //drill further down.
                node = child;
                x = coordinateInChild(this._xDiv, l, x);
                y = coordinateInChild(this._yDiv, l, y);
                l -= 1;

            }
        },

        _spawnChild: function () {
            var p = this._pyramidPool.retrieve();
            if (!p) {
                p = new Pyramid(this._xDiv, this._yDiv);
            }
            p._parent = this;
            p._pyramidPool = this._pyramidPool;
            return p;
        },

        contains: function (level, col, row) {
            return (row < this.yCountAtLevel(level) && col < this.xCountAtLevel(level) && col >= 0 && row >= 0);
        },

        xCountAtLevel: function (level) {
            return sizeAtLevel(level, this._yDiv);
        },
        yCountAtLevel: function (level) {
            return sizeAtLevel(level, this._xDiv);
        },
        parentX: function (level, colIndex) {
            return parentIndex(colIndex, this._xDiv);
        },
        parentY: function (level, rowIndex) {
            return parentIndex(rowIndex, this._yDiv);
        },

        parent: function (level, col, row, out_) {
            col = this.parentX(level, col);
            row = this.parentY(level, row);
            level = level - 1;
            out_.level = level;
            out_.row = row;
            out_.col = col;
        },
        xRangeAtLevel: function (level, index, deeperLevel, continuation) {
            return rangeAtDeeperLevel(level, index, this._xDiv, deeperLevel, continuation);
        },
        yRangeAtLevel: function (level, index, deeperLevel, continuation) {
            return rangeAtDeeperLevel(level, index, this._yDiv, deeperLevel, continuation);
        },
        boundsAtLevel: (function () {

            var tmp = {from: 0, to: 0};

            return function (level, colIndex, rowIndex, deeperLevel, out_) {

                this.xRangeAtLevel(level, colIndex, deeperLevel, tmp);
                out_.minX = tmp.from;
                out_.maxX = tmp.to;

                this.yRangeAtLevel(level, rowIndex, deeperLevel, tmp);
                out_.minY = tmp.from;
                out_.maxY = tmp.to;

            };

        }())

    };

    return Pyramid;

});