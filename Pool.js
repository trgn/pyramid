define([], function() {


  function Pool(options) {
    options = options || {};
    this._size = 0;
    this._limit = options.limit || 100;
    this._head = null;
  }

  Pool.prototype = {

    retrieve: function() {

      if (this._size === 0) {
        return;
      }

      var it = this._head;
      this._head = it._nextInPool;
      it._nextInPool = null;
      this._size -= 1;
      return it;

    },

    putBack: function(ob) {

      if (this._size >= this._limit) {//let the object evaporate in the thick mists of JS GC
        return;
      }

      ob._nextInPool = this._head;
      this._head = ob;
      this._size += 1;
    }


  };

  return Pool;


});